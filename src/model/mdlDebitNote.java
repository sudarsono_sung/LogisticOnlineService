package model;

public class mdlDebitNote {

	public String debitNoteNumber;
	public String customerID;
	public String orderManagementID;
	public String date;
	public Integer amount;
	public String status;
	public Integer finalRate;
	public Integer customClearance;
	public Integer ppn;
	public Integer pph23;
	
}
