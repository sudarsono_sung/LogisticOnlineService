package model;

import lombok.Data;

@Data
public class mdlLogBillingDepo {
	
	private String orderManagementID,
					note,
					urlProformaInvoice,
					transactionID,
					invoiceNumber,
					urlInvoice,
					urleFaktur,
					urlBonMuat,
					bonMuatId,
					message,
					paymentID, 
					journalNo, 
					jurnalDate;
	
	private int billingStatus, amount;
}
