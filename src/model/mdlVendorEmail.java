package model;

public class mdlVendorEmail {

	public String vendorOrderID;
	public String vendorOrderDetailID;
	public String driverID;
	public String driverName;
	public String vehicleNumber;
	public String orderType;
	public String itemDescription;
	public String customerID;
	public String customerName;
	public String email;
	public String cancelledReason;
	
}
