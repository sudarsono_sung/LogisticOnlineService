package model;

public class mdlDebitNoteDetail {
	
	private String debitNoteDetailID;
	private String debitNoteNumber;
	private Integer quantity;
	private Integer finalRate;
	private String description;
	
	public mdlDebitNoteDetail() {
		super();
	}

	public mdlDebitNoteDetail(String debitNoteDetailID, String debitNoteNumber, Integer quantity, Integer finalRate,
			String description) {
		super();
		this.debitNoteDetailID = debitNoteDetailID;
		this.debitNoteNumber = debitNoteNumber;
		this.quantity = quantity;
		this.finalRate = finalRate;
		this.description = description;
	}
	
	public String getDebitNoteDetailID() {
		return debitNoteDetailID;
	}
	public void setDebitNoteDetailID(String debitNoteDetailID) {
		this.debitNoteDetailID = debitNoteDetailID;
	}
	public String getDebitNoteNumber() {
		return debitNoteNumber;
	}
	public void setDebitNoteNumber(String debitNoteNumber) {
		this.debitNoteNumber = debitNoteNumber;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getFinalRate() {
		return finalRate;
	}
	public void setFinalRate(Integer finalRate) {
		this.finalRate = finalRate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
