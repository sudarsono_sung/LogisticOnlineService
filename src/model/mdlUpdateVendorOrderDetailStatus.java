package model;

public class mdlUpdateVendorOrderDetailStatus {
	public String vendorOrderDetailID; //M
	public String orderType; //M
	public Integer vendorDetailStatus; //M
	public String containerNumber;
	public String sealNumber;
	public Integer kmStart;
	public Integer kmEnd;
	public String driverCancelReason;
	public String updatedBy;
	public String datetime;
	public String signatureName;
	public String portID;
	public String tidNumber;
	public String kojaUrl;
	public String kojaUsername;
	public String kojaPassword;
}
