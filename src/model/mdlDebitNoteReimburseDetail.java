package model;

public class mdlDebitNoteReimburseDetail {
	
	private String debitNoteReimburseID;
	
	private String reimburseID;
	
	private String nameReimburse;
	
	private Integer reimburseAmount;
	
	private String debitNoteNumber;

	public String getDebitNoteReimburseID() {
		return debitNoteReimburseID;
	}

	public void setDebitNoteReimburseID(String debitNoteReimburseID) {
		this.debitNoteReimburseID = debitNoteReimburseID;
	}

	public String getNameReimburse() {
		return nameReimburse;
	}

	public void setNameReimburse(String nameReimburse) {
		this.nameReimburse = nameReimburse;
	}

	public Integer getReimburseAmount() {
		return reimburseAmount;
	}

	public void setReimburseAmount(Integer reimburseAmount) {
		this.reimburseAmount = reimburseAmount;
	}

	public String getDebitNoteNumber() {
		return debitNoteNumber;
	}

	public void setDebitNoteNumber(String debitNoteNumber) {
		this.debitNoteNumber = debitNoteNumber;
	}

	public String getReimburseID() {
		return reimburseID;
	}

	public void setReimburseID(String reimburseID) {
		this.reimburseID = reimburseID;
	}
	
	

}
