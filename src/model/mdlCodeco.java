package model;

import lombok.Data;

@Data
public class mdlCodeco {
	
	private String codecoID;
	private String containerNumber;
	private String isoCode;
	private String doNumber;
	private String depoOutDateTime;
	private String sealNumber;
	private String platNumber;
}
