package adapter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.mdlQueryExecute;

public class CreditNoteAdapter {

	public static String GetLastCreditNoteNumber(String orderType, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		String CreditNoteNumber = "";

		try {
			sql = "{call ws_CreditNoteNumberGetLast(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderType ));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetLastCreditNoteNumber");

			while(jrs.next()){
				CreditNoteNumber = jrs.getString("CreditNoteNumber");
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetLastCreditNoteNumber", sql, user);
		}

		return CreditNoteNumber;
	}
	
	public static String CreateCreditNoteNumber(String orderType, String user){
		String lastCreditNoteNumber = GetLastCreditNoteNumber(orderType, user);
		String dateNow = LocalDateTime.now().toString().substring(0, 10).toString();
		String datetime = ConvertDateTimeHelper.formatDate(dateNow, "yyyy-MM-dd", "yyyy/MM");
		String stringInc = "0001";

		if ((lastCreditNoteNumber != null) && (!lastCreditNoteNumber.equals("")) ){
			String[] partsCreditNoteNumber = lastCreditNoteNumber.split("/");
			String partDate = partsCreditNoteNumber[2] + "/" + partsCreditNoteNumber[3];
			String partNumber = partsCreditNoteNumber[4];

			//check if the month is same as today, if same, add 1 to the number
			if (partDate.equals(datetime)){
				int inc = Integer.parseInt(partNumber) + 1;
				stringInc = String.format("%04d", inc);
			}
		}
		StringBuilder sb = new StringBuilder();
		sb.append("CR/").append(orderType).append("/").append(datetime).append("/").append(stringInc);
		String CreditNoteNumber = sb.toString();
		return CreditNoteNumber;
	}
	
	public static void InsertCreditNote(model.mdlCreditNote param){
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		String sql = "";
		String orderType;
		if(param.orderManagementID.contains("E"))
			orderType = "EX";
		else
			orderType = "IM";
		
		try{
			String creditNoteNumber = CreateCreditNoteNumber(orderType, param.vendorID);
			
			sql = "{call ws_CreditNoteInsert(?,?,?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", creditNoteNumber));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.vendorID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.orderManagementID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.date));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", param.amount));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.status));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.vendorOrderID));

			QueryExecuteAdapter.QueryManipulate(sql, listParam, "ws_CreditNoteInsert");

		}catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_CreditNoteInsert", sql , param.vendorID);
		}

		return;
	}
	
}
