package adapter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import database.QueryExecuteAdapter;
import model.Globals;
import model.mdlDriverImage;
import model.mdlQueryExecute;

public class DriverImageAdapter {
    
    public static Boolean InsertDriverImage(List<model.mdlDriverImage> listParam){
        List<model.mdlQueryTransaction> listmdlQueryTransaction = new ArrayList<model.mdlQueryTransaction>();
        model.mdlQueryTransaction mdlQueryTransaction;
        
        //Globals.gCommand = "";
        String sql = "";
        Boolean success = false;
        try{
            for(mdlDriverImage param : listParam){
                mdlQueryTransaction = new model.mdlQueryTransaction();
                mdlQueryTransaction.sql = "{call ws_DriverImageInsert(?,?,?,?,?)}";
                mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", param.vendorOrderDetailID ));
                mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", LocalDateTime.now().toString() ));
                mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", param.path ));
                mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", param.driverID ));
                mdlQueryTransaction.listParam.add(QueryExecuteAdapter.QueryParam("string", param.type ));
                
                listmdlQueryTransaction.add(mdlQueryTransaction);
            }
            
            success = QueryExecuteAdapter.QueryTransaction(listmdlQueryTransaction, "InsertDriverImage");

        }catch(Exception ex){
            LogAdapter.InsertLogExc(ex.toString(), "InsertDriverImage", sql , listParam.get(0).driverID);
            //Globals.gReturn_Status = "Database Error";
        }

        return success;
    }
    
}
