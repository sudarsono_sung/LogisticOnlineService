package adapter;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import com.google.gson.Gson;

import database.QueryExecuteAdapter;
import helper.ConvertDateTimeHelper;
import model.ProductInvoice;
import model.mdlDebitNoteDetail;
import model.mdlDebitNoteReimburseDetail;
import model.mdlProductInvoiceSetting;
import model.mdlQueryExecute;
import model.mdlSettingInvoice;

public class DebitNoteAdapter {

	public static String GetLastDebitNoteNumber(String orderType, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		String DebitNoteNumber = "";

		try {
			sql = "{call ws_DebitNoteNumberGetLast(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderType ));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetLastDebitNoteNumber");

			while(jrs.next()){
				DebitNoteNumber = jrs.getString("DebitNoteNumber");
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetLastDebitNoteNumber", sql, user);
		}

		return DebitNoteNumber;
	}
	
	public static String CreateDebitNoteNumber(String orderType, String user){
		String lastDebitNoteNumber = GetLastDebitNoteNumber(orderType, user);
		String dateNow = LocalDateTime.now().toString().substring(0, 10).toString();
		String datetime = ConvertDateTimeHelper.formatDate(dateNow, "yyyy-MM-dd", "yyyy/MM");
		String stringInc = "0001";

		if ((lastDebitNoteNumber != null) && (!lastDebitNoteNumber.equals("")) ){
			String[] partsDebitNoteNumber = lastDebitNoteNumber.split("/");
			String partDate = partsDebitNoteNumber[2] + "/" + partsDebitNoteNumber[3];
			String partNumber = partsDebitNoteNumber[4];

			//check if the month is same as today, if same, add 1 to the number
			if (partDate.equals(datetime)){
				int inc = Integer.parseInt(partNumber) + 1;
				stringInc = String.format("%04d", inc);
			}
		}
		StringBuilder sb = new StringBuilder();
		sb.append("DN/").append(orderType).append("/").append(datetime).append("/").append(stringInc);
		String DebitNoteNumber = sb.toString();
		return DebitNoteNumber;
	}
	
	public static void InsertDebitNote(model.mdlDebitNote param){
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		String sql = "";
		String orderType;
		if(param.orderManagementID.contains("E"))
			orderType = "EX";
		else
			orderType = "IM";
		
		try{
			String debitNoteNumber = CreateDebitNoteNumber(orderType, param.customerID);
			param.debitNoteNumber = debitNoteNumber;
			
			sql = "{call ws_DebitNoteInsert(?,?,?,?,?,?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.debitNoteNumber));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.customerID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.orderManagementID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.date));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", param.amount));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.status));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", param.finalRate));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", param.customClearance));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", param.ppn));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", param.pph23));

			QueryExecuteAdapter.QueryManipulate(sql, listParam, "ws_DebitNoteInsert");

		}catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_DebitNoteInsert", sql , param.customerID);
		}

		return;
	}
	
	public static Integer GetDebitNoteAmount(String orderID, String user){
		Integer amount = 0;
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";

		try {
			sql = "{call sp_GetDebitNoteAmount(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderID ));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetDebitNoteAmount");

			while(jrs.next()){
				Double custRate = jrs.getDouble(2);
				Integer icustRate = custRate.intValue();
				
				amount += jrs.getInt(1) * icustRate;
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetDebitNoteAmount", sql, user);
		}

		return amount;
	}
	
	public static mdlSettingInvoice getSetingInvoiceByCusID(String orderManagementID, String user){
		mdlSettingInvoice settingInvoice = new mdlSettingInvoice();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";

		try {
			sql = "{call sp_GetSettingInvoiceByCusID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderManagementID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "GetSeetingInvoiceByCusID");

			while(jrs.next()){
				/*settingInvoice.setFinalRate((int) jrs.getDouble(6)); */
				settingInvoice.setFinalRate((int) jrs.getDouble(7) / (int) jrs.getDouble(5));
				settingInvoice.setCustomClearance((int) jrs.getDouble("CustomClearance"));
				settingInvoice.setPpn((int) jrs.getDouble("PPN"));
				settingInvoice.setPph23((int) jrs.getDouble("PPh23"));
				settingInvoice.setTruckingAmount((int) jrs.getDouble(7));
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetSeetingInvoiceByCusID", sql, user);
		}

		return settingInvoice;
	}
	
	public static List<mdlDebitNoteDetail> getSetingInvoiceByOrderManagementID(String orderManagementID, String debitNoteNumber, String user){
		List<mdlDebitNoteDetail> listDNDetail = new ArrayList<mdlDebitNoteDetail>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		
		CachedRowSet jrs = null;
		String sql = "";

		try {
			sql = "{call sp_GetSettingInvoiceByOrderManagementID(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderManagementID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "sp_GetSettingInvoiceByOrderManagementID");
			int no = 1;
			while(jrs.next()){
				mdlDebitNoteDetail dnDetail = new mdlDebitNoteDetail();
				dnDetail.setDebitNoteDetailID(debitNoteNumber +"-"+no);
				dnDetail.setDebitNoteNumber(debitNoteNumber);
				dnDetail.setQuantity(jrs.getInt("Quantity"));
				dnDetail.setFinalRate((int) jrs.getDouble("FinalRate"));
				dnDetail.setDescription(jrs.getString("Description"));
				
				listDNDetail.add(dnDetail);
				no++;
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetSeetingInvoiceByCusID", sql, user);
		}

		return listDNDetail;
	}
	
	public static void insertDebitNoteDetail(mdlDebitNoteDetail param, String user){
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		String sql = "";
		
		try{
			
			sql = "{call ws_DebitNoteDetailInsert(?,?,?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getDebitNoteDetailID()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getDebitNoteNumber()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", param.getQuantity()));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", param.getFinalRate()));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.getDescription()));

			QueryExecuteAdapter.QueryManipulate(sql, listParam, "ws_DebitNoteDetailInsert");

		}catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_DebitNoteDetailInsert", sql , user);
		}

		return;
	}
	
	public static List<model.mdlDebitNote> LoadDebitNote(){
		List<model.mdlDebitNote> listDebitNote = new ArrayList<model.mdlDebitNote>();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call ShowAllDebitNote()}";
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDebitNote");

			while(jrs.next()){
				model.mdlDebitNote mdl = new model.mdlDebitNote();
			    mdl.debitNoteNumber = 	jrs.getString("DebitNoteNumber");
			    mdl.orderManagementID = jrs.getString("OrderManagementID");
				listDebitNote.add(mdl);
			}
		}
		catch(Exception ex){
			// LogAdapter.InsertLogExc(ex.toString(), "LoadDebitNote", sql , customerID);
		}
		return listDebitNote;
	}
	
	public static model.mdlDebitNote LoadDebitNoteByDebitNoteNumber(String debitNoteNumber){
		model.mdlDebitNote mdl = new model.mdlDebitNote();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call ShowDebitNoteByDebitNoteNumber(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", debitNoteNumber ));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadDebitNoteByDebitNoteNumber");

			while(jrs.next()){
				
			    mdl.debitNoteNumber = 	jrs.getString("DebitNoteNumber");
			    mdl.orderManagementID = jrs.getString("OrderManagementID");
			}
		}
		catch(Exception ex){
			// LogAdapter.InsertLogExc(ex.toString(), "LoadDebitNote", sql , customerID);
		}
		return mdl;
	}
	
	// khusus untuk debit note
	public static model.mdlInvoice GetAmountTruckingAndDetail(String orderID, String user){
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		
		CachedRowSet jrs = null;
		String sql = "";
		model.mdlInvoice mdlInvoice = new model.mdlInvoice();
		mdlInvoice.truckingPrice = 0;
		mdlInvoice.totalParty = 0;

		try {
			sql = "{call sp_GetAmountTruckingAndDetail(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", orderID ));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "sp_GetAmountTruckingAndDetail");

			String pattern = "###,###";
			DecimalFormat decimalFormat = new DecimalFormat(pattern);
			String formatCustRate = "";
			
			int i = 0;
			while(jrs.next()){
				
				
				if(i==0){
					mdlInvoice.containerQty = jrs.getInt("Quantity") + " x " + jrs.getString("Description");
					
					formatCustRate = decimalFormat.format(jrs.getDouble("FinalRate")).replace(",", ".");
					mdlInvoice.detailTruckingPrice = jrs.getInt("Quantity") + " x " + formatCustRate;
				}
				else{
					mdlInvoice.containerQty += ", " + jrs.getInt("Quantity") + " x " + jrs.getString("Description");
					
					decimalFormat = new DecimalFormat(pattern);
					formatCustRate = decimalFormat.format(jrs.getDouble("FinalRate")).replace(",", ".");
					mdlInvoice.detailTruckingPrice += ", " + jrs.getInt("Quantity") + " x " + formatCustRate;
				}
				
				Double custRate = jrs.getDouble("FinalRate");
				Integer icustRate = custRate.intValue();
				mdlInvoice.truckingPrice += jrs.getInt("Quantity") * icustRate;
				
				mdlInvoice.totalParty += jrs.getInt("Quantity");
				i++;
			}
			
			
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "GetDebitNoteAmount", sql, user);
		}

		return mdlInvoice;
	}
		
	public static List<mdlDebitNoteReimburseDetail> loadDebitNoteReimburseDetail(String debitNoteNumber){
		List<mdlDebitNoteReimburseDetail> listDebitNoteReimburseDetail = new ArrayList<>();
		List<model.mdlQueryExecute> listParam = new ArrayList<>();
		
		CachedRowSet jrs = null;
		String sql = "";
		
		try{
			sql = "{call sp_LoadReimburseDetailByDebitNoteNumber(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", debitNoteNumber));
			
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "loadDebitNoteReimburseDetail");

			while(jrs.next()){
				mdlDebitNoteReimburseDetail mdlDebitNoteReimburseDetail = new mdlDebitNoteReimburseDetail();
				mdlDebitNoteReimburseDetail.setDebitNoteReimburseID(jrs.getString("DebitNoteReimburseID"));
				mdlDebitNoteReimburseDetail.setNameReimburse(jrs.getString("Name"));
				mdlDebitNoteReimburseDetail.setReimburseAmount((int) jrs.getDouble("ReimburseAmount"));
				listDebitNoteReimburseDetail.add(mdlDebitNoteReimburseDetail);
			}
		}catch (Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "loadDebitNoteReimburseDetail", sql , debitNoteNumber);
		}
		
		return listDebitNoteReimburseDetail;
		
		
		
	}
	
	public static mdlProductInvoiceSetting LoadProductInvoiceSetting(String debitNoteNumber, String user){
		mdlProductInvoiceSetting mdlProductInvoiceSetting = new mdlProductInvoiceSetting();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		try{
			
			sql = "{call sp_LoadProductInvoiceSetting(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", debitNoteNumber));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadProductInvoiceSetting");

			while(jrs.next()){
				mdlProductInvoiceSetting.setCustomerName(jrs.getString("Name"));
				mdlProductInvoiceSetting.setProductInvoice(jrs.getString("ProductInvoice"));
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadProductInvoiceSetting", sql , user);
		}
		return mdlProductInvoiceSetting;
	}
	
	
	public static String updateFinalRateByDebitNoteNumberDetailID(String debitNoteNumber, Integer amount, String user)
	{
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		String result = "";

		try{
			sql = "{call sp_UpdateAmountDebitNoteByDebitNoteNumber(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", debitNoteNumber));
			listParam.add(QueryExecuteAdapter.QueryParam("integer", amount));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "updateFinalRateByDebitNoteNumberDetailID");

			result = success == true ? "Success Update Amount" : "Database Error";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "updateFinalRateByDebitNoteNumberDetailID", sql , user);
			result = "Database Error";
		}

		return result;
	}
	
	public static int amountInvoice(String debitNoteNumber, String user){
		model.mdlInvoice mdlInvoice = new model.mdlInvoice();
		List<model.mdlQueryExecute> listParam = new ArrayList<model.mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		int customeClearance = 0, persenPPN = 0, persenPPh23 = 0, totalPPh23=0;
		try{
			sql = "{call sp_DebitNoteLoadInvoice(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", debitNoteNumber));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "LoadInvoice");

			while(jrs.next()){
				mdlInvoice.setDebitNoteNumber(jrs.getString("DebitNoteNumber"));
				mdlInvoice.setCustomerID(jrs.getString("CustomerID"));
				mdlInvoice.setCustomer(jrs.getString(4));
				
				if(jrs.getString("BillingAddress") == null || jrs.getString("BillingAddress").equals(""))
					mdlInvoice.setAddress(jrs.getString("Address"));
				else
					mdlInvoice.setAddress(jrs.getString("BillingAddress"));
				
				mdlInvoice.setPhone(jrs.getString("MobilePhone") + " / " + jrs.getString("OfficePhone"));
				mdlInvoice.setEmail(jrs.getString("Email"));
				mdlInvoice.setOrderID(jrs.getString("OrderManagementID"));
				mdlInvoice.setInvoiceDate(ConvertDateTimeHelper.formatDate(jrs.getString("Date"), "yyyy-MM-dd", "dd MMM yyyy"));
				mdlInvoice.setDueDate(ConvertDateTimeHelper.formatDate(jrs.getString(10), "yyyy-MM-dd", "dd MMM yyyy"));
				mdlInvoice.setSiNumber(jrs.getString("ShippingInvoiceID"));
				mdlInvoice.setVesselVoyage(jrs.getString("VesselName") + "/" + jrs.getString("VoyageNo"));
				mdlInvoice.setRoute(jrs.getString(14) + " - " + jrs.getString("PortName") + "(" + jrs.getString("PortUTC") +")");
				
				model.mdlInvoice debitNoteAmountDetail = GetAmountTruckingAndDetail(jrs.getString("OrderManagementID"), user);
				mdlInvoice.setContainerQty(debitNoteAmountDetail.containerQty);
				mdlInvoice.setDetailTruckingPrice(debitNoteAmountDetail.detailTruckingPrice);
//				mdlInvoice.setTruckingPrice(debitNoteAmountDetail.truckingPrice);
				mdlInvoice.setTotalParty(debitNoteAmountDetail.totalParty);
				
				
				persenPPh23 = jrs.getInt("PPh23");
				persenPPN = jrs.getInt("PPN");
				mdlInvoice.setPph23(persenPPh23);
				mdlInvoice.setPpn(persenPPN);
				
				
				
				List<mdlDebitNoteReimburseDetail> listReimburseDetail = loadDebitNoteReimburseDetail(debitNoteNumber);
				
				Integer totalReimburse = 0;
				for (mdlDebitNoteReimburseDetail ls : listReimburseDetail) {
					totalReimburse += ls.getReimburseAmount();
				}
				
				mdlInvoice.setAdditionalPrice((int) jrs.getDouble("AdditionalPrice"));
				mdlInvoice.setFinalRate((int) jrs.getDouble("FinalRate"));;
				
				
				mdlProductInvoiceSetting productInvoiceSetting = DebitNoteAdapter.LoadProductInvoiceSetting(debitNoteNumber, user);
				String productInvoiceName = productInvoiceSetting.getProductInvoice();
				
				if(productInvoiceName.equals(ProductInvoice.PACKETA.toString())) {
					mdlInvoice.setProductInvoice("A");
					
					int totalCustomeClearance = (int)jrs.getDouble("CustomClearance") * mdlInvoice.getTotalParty();
					mdlInvoice.setCustomClearance(totalCustomeClearance);
					
					// amount trucking
					mdlInvoice.setTruckingPrice(debitNoteAmountDetail.truckingPrice);
					
					mdlInvoice.setPpnCustom(((debitNoteAmountDetail.truckingPrice + totalCustomeClearance) * persenPPN)/100);
					
					mdlInvoice.setSubTotal(mdlInvoice.getTruckingPrice() + mdlInvoice.getCustomClearance() + mdlInvoice.getPpnCustom());
					
					totalPPh23 = (((debitNoteAmountDetail.truckingPrice + totalCustomeClearance) * persenPPh23)/100);
					mdlInvoice.setTotalPPh23(totalPPh23);
					mdlInvoice.setTotal(mdlInvoice.getSubTotal() - totalPPh23);
	
					mdlInvoice.setListReimburseDetail(listReimburseDetail);
					mdlInvoice.setTotalReimburse(totalReimburse);
				}else if (productInvoiceName.equals(ProductInvoice.PACKETB.toString())) {
					mdlInvoice.setProductInvoice("B");
					System.out.println(debitNoteAmountDetail.truckingPrice);
					
					totalPPh23 = (mdlInvoice.getFinalRate() * persenPPh23)/100;
					
					mdlInvoice.setTotalPPh23(totalPPh23);
					
					// amount trucking
					mdlInvoice.setTruckingPrice(mdlInvoice.getFinalRate());
					System.out.println("hai "+mdlInvoice.getCustomClearance());
					
					mdlInvoice.setPpnCustom((mdlInvoice.getTruckingPrice() * persenPPN)/100);
					
					mdlInvoice.setSubTotal(mdlInvoice.getTruckingPrice() + mdlInvoice.getPpnCustom());
					mdlInvoice.setTotal(mdlInvoice.getSubTotal() - totalPPh23);
					
	
//					mdlInvoice.setListReimburseDetail(listReimburseDetail);
					mdlInvoice.setTotalReimburse(0);
				}else if (productInvoiceName.equals(ProductInvoice.PACKETC.toString())) {
					mdlInvoice.setProductInvoice("C");
					System.out.println(debitNoteAmountDetail.truckingPrice);
					
					totalPPh23 = (mdlInvoice.getFinalRate() * persenPPh23)/100;
					
					mdlInvoice.setTotalPPh23(totalPPh23);
					
					// amount trucking
					mdlInvoice.setTruckingPrice(mdlInvoice.getFinalRate());
					System.out.println("hai "+mdlInvoice.getCustomClearance());
					
					mdlInvoice.setPpnCustom((mdlInvoice.getTruckingPrice() * persenPPN)/100);
					
					mdlInvoice.setSubTotal(mdlInvoice.getTruckingPrice() + mdlInvoice.getPpnCustom());
					mdlInvoice.setTotal(mdlInvoice.getSubTotal() - totalPPh23);
					
	
//					mdlInvoice.setListReimburseDetail(listReimburseDetail);
					mdlInvoice.setTotalReimburse(0);
				}else if (productInvoiceName.equals(ProductInvoice.PACKETD.toString())) {
					mdlInvoice.setProductInvoice("D");
					
					int totalCustomeClearance = (int)jrs.getDouble("CustomClearance") * mdlInvoice.getTotalParty();
					mdlInvoice.setCustomClearance(totalCustomeClearance);
					
					totalPPh23 = (totalCustomeClearance * persenPPh23)/100;
					
					mdlInvoice.setTotalPPh23(totalPPh23);
					
					mdlInvoice.setPpnCustom((totalCustomeClearance * persenPPN)/100);
					
					mdlInvoice.setSubTotal(totalCustomeClearance + mdlInvoice.getPpnCustom());
					mdlInvoice.setTotal(mdlInvoice.getSubTotal() - totalPPh23);
					
	
//					mdlInvoice.setListReimburseDetail(listReimburseDetail);
					mdlInvoice.setTotalReimburse(0);
				}
				
				
				
				
				
				mdlInvoice.setGrandTotal(mdlInvoice.getTotal() + mdlInvoice.getTotalReimburse() + mdlInvoice.getAdditionalPrice());
				
				
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "AmountInvoice", sql , user);
		}
		return mdlInvoice.getGrandTotal();
	}
	
}
