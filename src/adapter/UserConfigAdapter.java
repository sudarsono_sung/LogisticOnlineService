package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.Globals;
import model.mdlQueryExecute;

public class UserConfigAdapter {
	public static model.mdlUserConfig GetUserConfig(String deviceID){
		model.mdlUserConfig mdlUserConfig = new model.mdlUserConfig();

		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		CachedRowSet jrs = null;
		//Globals.gCommand="";
		String sql = "";
		try{
			sql = "{call sp_GetUserConfig(?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", deviceID));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "ws_GetUserConfig");

			while(jrs.next()){
				mdlUserConfig.driverID = jrs.getString("DriverID");
				mdlUserConfig.driverName = jrs.getString(2);
				mdlUserConfig.vendorID = jrs.getString("VendorID");
				mdlUserConfig.vendorName = jrs.getString(4);
				mdlUserConfig.ipLocal = jrs.getString("IpLocal");
				mdlUserConfig.portLocal = jrs.getString("PortLocal");
				mdlUserConfig.ipPublic = jrs.getString("IpPublic");
				mdlUserConfig.portPublic = jrs.getString("PortPublic");
				mdlUserConfig.ipAlternative = jrs.getString("IpAlternative");
				mdlUserConfig.portAlternative = jrs.getString("PortAlternative");
				mdlUserConfig.password = jrs.getString("Password");
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_GetUserConfig", sql , deviceID);
		}
		return mdlUserConfig;
	}

}
