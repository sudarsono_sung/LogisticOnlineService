package adapter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import database.QueryExecuteAdapter;
import model.Globals;
import model.mdlQueryExecute;

public class LiveTrackingAdapter {
	public static Boolean InsertLiveTracking(model.mdlLiveTracking mdlLiveTracking ){
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		//Globals.gCommand = "";
		String sql = "";
		Boolean success = false;
		try{
			String dateNow = LocalDateTime.now().toString();
			sql = "{call sp_LiveTrackingInsert(?,?,?,?,?, ?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", dateNow));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlLiveTracking.vehicleNumber));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlLiveTracking.vendorID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlLiveTracking.driverID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlLiveTracking.latitude));
			listParam.add(QueryExecuteAdapter.QueryParam("string", mdlLiveTracking.longitude));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "ws_InsertLiveTracking");

		}catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_InsertLiveTracking", sql , mdlLiveTracking.driverID);
			//Globals.gReturn_Status = "Database Error";
		}

		return success;
	}
}
