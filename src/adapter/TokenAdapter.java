package adapter;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.CachedRowSet;

import database.QueryExecuteAdapter;
import model.mdlQueryExecute;

public class TokenAdapter {

	public static Boolean UpdateToken(model.mdlLogin param){
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		String sql = "";
		Boolean success = false;
		try{
			sql = "{call ws_InsertUpdateToken(?,?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.username));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.password));
			listParam.add(QueryExecuteAdapter.QueryParam("string", param.token));

			success = QueryExecuteAdapter.QueryManipulate(sql, listParam, "ws_InsertUpdateToken");

		}catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_InsertUpdateToken", sql , param.username);
		}

		return success;
	}
	
	public static Boolean CheckTokenValidity(String driverID, String token){
		Boolean check = false;
		List<mdlQueryExecute> listParam = new ArrayList<mdlQueryExecute>();
		CachedRowSet jrs = null;
		String sql = "";
		try{
			sql = "{call ws_CheckDriverToken(?,?)}";
			listParam.add(QueryExecuteAdapter.QueryParam("string", driverID));
			listParam.add(QueryExecuteAdapter.QueryParam("string", token));
			jrs = QueryExecuteAdapter.QueryExecute(sql, listParam, "ws_CheckDriverToken");
			while(jrs.next()){
				check = true;
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "ws_CheckDriverToken", sql , driverID);
		}
		return check;
	}
	
}
